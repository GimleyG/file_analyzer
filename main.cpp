#include <iostream>
#include <iomanip>
#include <map>

#include "file_analyzer.h"
#include "input_parser.h"

void printUsage() {
	std::cout << "============================" << std::endl
		<< "A file analyzer that counts a number of particular word entry and a checksum." << std::endl
		<< "Version 1.0. Created by GimleyG. 2017" << std::endl
		<< "============================" << std::endl
		<< "Usage: analyzer [-h] -f filename [-m words [-v query_word] | -m checksum] " << std::endl
		<< std::endl
		<< "Options:" << std::endl
		<< "-h           \tPrint this help" << std::endl
		<< "-f filename  \tName of file to analyze" << std::endl
		<< "-m <mode>:   \tMode" << std::endl
		<< "   words     \ta number of particular word is counted" << std::endl
		<< "   checksum   \ta checksum is counted as a sum of 32-bit words that represent a file data" << std::endl
		<< "-v query_word  \tA word that program will seek for in a file in the 'words' mode" << std::endl;			
}

int main(int argc, char *argv[]) {	
	InputParser ip;
	try {
		ip.analyzeInput(argc, argv);
	} catch(std::logic_error e) {
		std::cerr << "An error occured during command line oprions analyze: " << e.what() << std::endl;
		exit(1);
	}

	if (ip.calledHelp()) {
		printUsage();
		return 0;
	}
		
	FileAnalyzer fa(ip.getFlagValue(FlagType::FILE_NAME));

	try {
		if (ip.getFlagValue(FlagType::MODE) == "words")
			std::cout << "Found matches: " << fa.findWordEntryCount(ip.getFlagValue(FlagType::QUERY_WORD)) << std::endl;
		else
			std::cout << "Checksum is: " << std::hex << std::setw(8) << std::setfill('0') << fa.getChecksum() << std::endl;
	} catch (std::logic_error e) {
		std::cerr << "An error occured during file analyze: " << e.what() << std::endl;
		exit(1);
	}
    return 0;
}
