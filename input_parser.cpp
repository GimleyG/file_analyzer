#include "input_parser.h"

void InputParser :: analyzeInput(int &argc, char **argv) {
	if (argc < 2 )
		throw std::logic_error("too few parameters");

	std::string fst_flag(argv[1]);

	if (fst_flag == "-f" && argc > 2) {
		params_.insert({ FlagType::FILE_NAME, std::string(argv[2]) });
		if (argc > 4 && std::string(argv[3]) == "-m") {
			std::string mode(argv[4]);
			if (mode == "words" || mode == "checksum")
				params_.insert({ FlagType::MODE, std::string(argv[4]) });
			else {
				std::string msg("an unknown mode name ");
				msg += mode;
				throw std::logic_error(msg);
			}
			if (mode == "words") {
				if (argc > 6 && std::string(argv[5]) == "-v") {
					params_.insert({ FlagType::QUERY_WORD, std::string(argv[6]) });
				} else {
					throw std::logic_error("a query word is not set");
				}
			}
		} else {
			throw std::logic_error("an unknown flag");
		}		
	}
	else if (fst_flag == "-h")
		needHelp_ = true;
	else
		throw std::logic_error("an unknown flag");
}

std::string InputParser :: getFlagValue(FlagType type) {
	return params_[type];
}
