#pragma once

#include <map>

enum class FlagType {
	FILE_NAME = 1,
	MODE,
	QUERY_WORD
};

/**
* A class to store input program parameters.
* Copying, assignment, moving are forbidden.
*/
class InputParser {
public:
	InputParser() : needHelp_(false) {}
	InputParser(const InputParser&) = delete;
	InputParser(const InputParser&&) = delete;
	InputParser& operator=(const InputParser&) = delete;
	InputParser& operator=(const InputParser&&) = delete;


	void analyzeInput(int &argc, char **argv);
	std::string getFlagValue(FlagType type);

	bool calledHelp() { return needHelp_; }

private:
	std::map<FlagType, std::string> params_;
	bool needHelp_;
};