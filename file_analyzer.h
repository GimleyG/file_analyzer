#pragma once

#include <string>

char const word_length = 32;

/**
* A class to analyze file
*/
class FileAnalyzer {
public:
	FileAnalyzer(std::string filename) : filename_{ filename } {}
	~FileAnalyzer() {}

	int findWordEntryCount(const std::string &query_word);
	unsigned long long getChecksum();
private:
	std::string filename_;
};
