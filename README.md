A console program that analyzes an input file.
Has 2 modes: counts particular word in the file; counts a 32-bit checksum.
Run with '-h' parameter to see a help message.