#include "file_analyzer.h"

#include <bitset>
#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>
#include <vector>

template <unsigned int N>
void bitsetAdd(std::bitset<N>& x, const std::bitset<N>& y) {
	bool carry = false;
	for (int i = 0; i < N ; ++i) {
		bool xi = x[i];
		x[i] = (x[i] ^ y[i]) ^ carry;
		carry = (xi && y[i]) || (xi && carry) || (y[i] && carry);
	}
}

int FileAnalyzer :: findWordEntryCount(const std::string &query_word) {
	int count = 0;
	std::ifstream stream(filename_, std::ios::in);
	if (stream.is_open()) {
		std::string line;

		while (std::getline(stream, line)) {
			if (line.find(query_word) != std::string::npos) {
				std::istringstream iss(line);
				std::vector<std::string> words{ std::istream_iterator<std::string>{iss},
					std::istream_iterator<std::string>{} };
				for (auto t : words) {
					if (t == query_word)
						count++;
				}
			}
		}
		stream.close();
	} else {
		throw std::logic_error("No such file");
	}
	return count;
}

unsigned long long FileAnalyzer :: getChecksum() {
	std::ifstream stream(filename_, std::ios::in | std::ios::binary);
	std::bitset<word_length> bs;

	if (stream.is_open()) {
		stream.seekg(0, stream.end);
		unsigned int length = stream.tellg();
		stream.seekg(0, stream.beg);

		unsigned int currentPos = 0;
		std::bitset<word_length> buffer;
		while (currentPos < length) {						
			stream.read(reinterpret_cast<char*>(&buffer), 4);				
			bitsetAdd<word_length>(bs, buffer);			
			buffer.reset();
			
			currentPos += 4;
			stream.seekg(currentPos);			
		}	
		stream.close();
	}
	else {
		throw std::logic_error("No such file");
	}	
	return bs.to_ullong();
}

